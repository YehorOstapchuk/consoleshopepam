﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace shopEPAM
{
    public static class Tools
    {
        public static GuestArgs GetFindData()
        {
            GuestArgs args = new GuestArgs();
            Console.WriteLine("Enter the name of good");
            args.Find = Console.ReadLine();
            return args;
        }


        public static GuestArgs GetDataToChangeProfile()
        {
            GuestArgs args = new GuestArgs();
            bool flag = true;
            int key;
            while(flag)
            {
                Console.WriteLine("Choose what you want to change:");
                Console.WriteLine("1 - FirstName" +
                    "2 - SecondName" +
                    "3 - NickName" +
                    "4 - Password" +
                    "5 - PhoneNumber" +
                    "6 - SaveChanges");
                key = Int32.Parse(Console.ReadLine());
                switch(key)
                {
                    case 1: args.FirstName = Change();
                        break;
                    case 2: args.SecondName = Change();
                        break;
                    case 3: string temp = Change();
                        if (ShopDbMock.Users.Where(a => a.NickName == temp).ToArray().Length > 0) Console.WriteLine("This NickName is reserved.");
                        else args.NickName = temp;
                        break;
                    case 4: args.Password = Change();
                        break;
                    case 5: args.PhoneNumber = Change();
                        break;
                    case 6: flag = false;
                        break;
                }
            }
            return args;
        }


        public static AdminArgs GetDataToChangeGood()
        {
            AdminArgs args = new AdminArgs();
            bool flag = true;
            int key;
            while (flag)
            {
                Console.WriteLine("Choose what you want to change:");
                Console.WriteLine("1 - Name" +
                    "2 - Category" +
                    "3 - Description" +
                    "4 - Price" +
                    "5 - Save Changes");
                key = Int32.Parse(Console.ReadLine());
                switch (key)
                {
                    case 1:
                        args.GoodName = Change();
                        break;
                    case 2:
                        args.GoodCategory = Change();
                        break;
                    case 3:
                        args.GoodDescription = Change();
                        break;
                    case 4:
                        args.GoodPrice = Decimal.Parse(Change());
                        break;
                    case 5:
                        flag = false;
                        break;
                }
            }
            return args;
        }


        public static AdminArgs GetDataToChangeOrderStatus()
        {
            AdminArgs args = new AdminArgs();

            int key;
                Console.WriteLine("Choose what you want to change:");
                Console.WriteLine("1 - Sending" +
                    "2 - Sent" +
                    "3 - OnWay" +
                    "4 - DeliveredToPost" +
                    "5 - DeniedByAdmin");
                key = Int32.Parse(Console.ReadLine());
                switch (key)
                {
                    case 1:
                    args.OrderStatus = OrderStatus.Sending;
                        break;
                    case 2:
                    args.OrderStatus = OrderStatus.Sent;
                    break;
                    case 3:
                    args.OrderStatus = OrderStatus.OnWay;
                    break;
                    case 4:
                    args.OrderStatus = OrderStatus.DeliveredToPost;
                    break;
                    case 5:
                    args.OrderStatus = OrderStatus.DeniedByAdmin;
                    break;
                default:
                    args.OrderStatus = OrderStatus.DeniedByAdmin;
                    break;
            }
            
            return args;
        }

        public static void ChangeOrder(GuestArgs args, User user)
        {
            if (args.FirstName != null) user.ChangeFirstName(args.FirstName);
            if (args.SecondName != null) user.ChangeSecondName(args.SecondName);
            if (args.NickName != null) user.ChangeNickName(args.NickName);
            if (args.Password != null) user.ChangePassword(args.Password);
            if (args.PhoneNumber != null) user.ChangePhone(args.PhoneNumber);
        }

        public static void ChangeGood(AdminArgs args, Good good)
        {
            if (args.GoodName != null) good.ChangeName(args.GoodName);
            if (args.GoodCategory != null) good.ChangeCategory(args.GoodCategory);
            if (args.GoodDescription != null) good.ChangeDescription(args.GoodDescription);
            if (args.GoodPrice > 0) good.ChangePrice(args.GoodPrice);
        }


        public static void ChangeUser(GuestArgs args, User user)
        {
            if (args.FirstName != null) user.ChangeFirstName(args.FirstName);
            if (args.SecondName != null) user.ChangeSecondName(args.SecondName);
            if (args.NickName != null) user.ChangeNickName(args.NickName);
            if (args.Password != null) user.ChangePassword(args.Password);
            if (args.PhoneNumber != null) user.ChangePhone(args.PhoneNumber);
        }

        private static string Change ()
        {
            Console.WriteLine("Enter new one:");
            string temp = Console.ReadLine();
            return temp;
        }


        public static IEnumerable<Good> GetAllGoods ()
        {
            return ShopDbMock.Goods;
        }

        public static IEnumerable<Order> GetAllOrders()
        {
            return ShopDbMock.Orders;
        }

        public static IEnumerable<User> GetAllUsers()
        {
            return ShopDbMock.Users;
        }

        public static IEnumerable<Good> FindGood (GuestArgs args)
        {
            foreach(Good good in ShopDbMock.Goods)
            {
                if (good.Name == args.Find) yield return good;
            }
        }

        public static IEnumerable<Order> GetAllUserOrders(int userId)
        {
            return ShopDbMock.Orders.Where(a => a.UserId == userId).ToList();
        }


        public static IEnumerable<Order> GetAllUserOrdersWithStatus(int userId, OrderStatus status)
        {
            return ShopDbMock.Orders.Where(a => (a.UserId == userId)&&(a.Status == status)).ToList();
        }

        public static Good FindByIdGood(int id)
        {
            return ShopDbMock.Goods.Where(a => a.Id == id).FirstOrDefault();
        }
        public static Order FindByIdOrder(int id)
        {
            return ShopDbMock.Orders.Where(a => a.Id == id).FirstOrDefault();
        }


        public static void DisplayOrderForUser(Order order)
        {
            Console.WriteLine("-------------------------------------");
            Console.WriteLine("OrderId:" + order.Id);
            Console.WriteLine("Good's naming:" + FindByIdGood(order.GoodId).Name);
            Console.WriteLine("Good's price:" + FindByIdGood(order.GoodId).Price);
            Console.WriteLine("Order Status:" + order.Status);
        }


        public static void DisplayOrderForAdmin(Order order)
        {
            Console.WriteLine("-------------------------------------");
            Console.WriteLine("OrderId:" + order.Id);
            Console.WriteLine("Customer First Name:" + ShopDbMock.Users.Where(a => a.Id == order.UserId).FirstOrDefault().FirstName);
            Console.WriteLine("Customer Second Name:" + ShopDbMock.Users.Where(a => a.Id == order.UserId).FirstOrDefault().SecondName);
            Console.WriteLine("Good's naming:" + FindByIdGood(order.GoodId).Name);
            Console.WriteLine("Good's price:" + FindByIdGood(order.GoodId).Price);
            Console.WriteLine("Order Status:" + order.Status);
        }


        public static void DisplayGood(Good goood)
        {
            Console.WriteLine("--------------------------------------");
            Console.WriteLine("Good ID:" + goood.Id);
            Console.WriteLine("Good Name:" + goood.Name);
            Console.WriteLine("Good Description:" + goood.Description);
            Console.WriteLine("Good Price:" + goood.Price);
        }
        public static void DisplayUser(User user)
        {
            Console.WriteLine("--------------------------------------");
            Console.WriteLine("User ID:" + user.Id);
            Console.WriteLine("User FirstName:" + user.FirstName);
            Console.WriteLine("User SecondName:" + user.SecondName);
            Console.WriteLine("User NickName:" + user.NickName);
            Console.WriteLine("User Password:" + user.Password);
            Console.WriteLine("User Phone:" + user.PhoneNumber);
            Console.WriteLine("User Status:" + user.Status.ToString());
        }

        public static void ShowGoods()
        {
            Console.WriteLine("Goods-List:");
            var goods = GetAllGoods();
            foreach(var good in goods)
            {
                DisplayGood(good);
            }
           // Console.WriteLine("--------------------------------------");
        }

        public static void ShowFindGood(GuestArgs args)
        {
            IEnumerable<Good> goods = FindGood(args);
            if (goods.Count() == 0) Console.WriteLine("NOT FOUND");
            else
            {
                foreach (var good in goods)
                {
                    DisplayGood(good);
                }
               // Console.WriteLine("--------------------------------------");
            }
        }
    }
}
