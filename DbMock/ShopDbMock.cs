﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace shopEPAM
{
    public static class ShopDbMock
    {

        public static int UserAmount
        {
            get;
            set;
        }

        public static int GoodsAmount
        {
            get;
            set;
        }

        public static int OrdersAmount
        {
            get;
            set;
        }

        public static List<User> Users
        {
            get;
            set;
        }
        public static List<Good> Goods
        {
            get;
            set;
        }
        public static List<Order> Orders
        {
            get;
            set;
        }

        static ShopDbMock ()
        {
            Users = new List<User>();
            Goods = new List<Good>();
            Orders = new List<Order>();
            UserAmount = 0;
            GoodsAmount = 0;
            OrdersAmount = 0;
        }

        public static void AddUser(string firstName, string secondName, string nickname, string password, string phone = null, UserType status = UserType.Registered)
        {
            User temp = User.Create(firstName, secondName, nickname, password, phone, status);
            if ((temp != null) && (UsersAddValidation(temp))) { Users.Add(temp); UserAmount++; }
            else throw new ArgumentException();
        }

        public static void AddGood(string name, decimal price, string category = null, string description = null)
        {
            Good temp = Good.Create(name, price, category, description);
            if ((temp != null) && (GoodsAddValidation(temp))) { Goods.Add(temp); GoodsAmount++; }
            else throw new ArgumentException();
        }

        public static void AddOrder(User user, Good good)
        {
            Order temp = Order.Create(user, good);
            if ((temp != null) && (OrdersAddValidation(temp))) { Orders.Add(temp); OrdersAmount++; }
            else throw new ArgumentException();
        }

        public static void AddUser(User user)
        {
            if ((user != null) && (UsersAddValidation(user))) { Users.Add(user); UserAmount++; }
            else throw new ArgumentException();
        }

        public static void AddGood(Good good)
        {
            if ((good != null) && (GoodsAddValidation(good)))
            {
                Goods.Add(good); GoodsAmount++;
            }
            else throw new ArgumentException();
        }

        public static void AddOrder (Order order)
        {
            if ((order != null) && (OrdersAddValidation(order)))
            {
                Orders.Add(order); OrdersAmount++;
            }
            else throw new ArgumentException();
        }

        public static void DeleteUser (User user)
        {
            if (user != null) { if (!RemoveValidation(user.Id, Users.Select( a => a.Id).ToArray())) throw new ArgumentException("This element doesnt exist in db.");
               Users.Remove(user); UserAmount--; }
            else throw new ArgumentException("User fields validation failed.");
        }

        public static void DeleteGood(Good good)
        {
            if (good != null)
            {
                Goods.Remove(good); GoodsAmount--;
            }
            else throw new ArgumentException();
        }

        public static void DeleteOrder(Order order)
        {
            if (order != null)
            {
                Orders.Remove(order); OrdersAmount--;
            }
            else throw new ArgumentException();
        }

        private static bool UsersAddValidation (User user)
        {
            if ((Users.Where(a => a.Id == user.Id).Count() > 0) || (Users.Where(b => (b.NickName == user.NickName)).Count() > 0)) return false;
            else return true;
        }

        private static bool GoodsAddValidation (Good good)
        {
            if ((Goods.Where(a => a.Id == good.Id).Count() > 0) || (Goods.Where(b => (b.Name == good.Name) && (b.Description == good.Description) && (b.Price == good.Price) ).Count() > 0)) return false;
            else return true;
        }

        private static bool OrdersAddValidation (Order order)
        {
            if (Orders.Where(a => a.Id == order.Id).Count() > 0) return false;
            else return true;
        }

        private static bool RemoveValidation (int id, int [] arr)
        {
            if (arr.Contains(id)) return true;
            else return false;
        }
    }
}
