﻿using System;
using System.Collections.Generic;
using System.Text;

namespace shopEPAM
{
   public class Order
    {
        public static int LastNumber = -1;

        public int Id
        {
            get;
           private set;
        }

        public int UserId
        {
            get;
           private set;
        }

        public int GoodId
        {
            get;
           private set;
        }

        public OrderStatus Status
        {
            get;
           private set;
        }

        private Order (int userId, int goodId, OrderStatus status )
        {
            LastNumber++;
            Id = LastNumber;
            UserId = userId;
            GoodId = goodId;
            Status = status;
        }

        public static Order Create(User user, Good good)
        {
            if (good == null) throw new ArgumentException("Good not found");
            return new Order(user.Id, good.Id, OrderStatus.New);

        }

        public bool ChangeStatus (OrderStatus status)
        {
            if (StatusValidation(status))
            {
                Status = status;
                return true;
            }
            else return false;
            
        }

        public bool StatusValidation(OrderStatus status)
        {
            return true;
        }
    }
}
