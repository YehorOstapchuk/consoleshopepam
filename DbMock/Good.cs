﻿using System;
using System.Collections.Generic;
using System.Text;

namespace shopEPAM
{
    public class Good
    {
        public static int LastNumber = -1;
        public int Id
        {
            get;
            private set;
        }

        public string Name
        {
            get;
            private set;
        }

        public string Category
        {
            get;
           private set;
        }

        public string Description
        {
            get;
           private set;
        }

        public decimal Price
        {
            get;
            private set;
        }

        private Good (string name, decimal price, string category, string description)
        {
            LastNumber++;
            Id = LastNumber;
            Name = name;
            Category = category;
            Description = description;
            Price = price;
        }

        public static Good Create (string name, decimal price, string category = null, string description = null)
        {
            if (Validation(name, price, category, description)) return new Good(name, price, category, description);
            else return null;
        }

        public bool ChangeName (string name)
        {
            if (NameValidation(name)) { Name = name; return true; }
            else return false;
        }

        public bool ChangeDescription(string description)
        {
            if (DesriptionValidation(description)) { Name = description; return true; }
            else return false;
        }

        public bool ChangeCategory(string category)
        {
            if (CategoryValidation(category)) { Name = category; return true; }
            else return false;
        }

        public bool ChangePrice(decimal price)
        {
            if (PriceValidation(price)) { Price = price; return true; }
            else return false;
        }



        private static bool Validation(string name, decimal price, string category, string description)
        {
            return (NameValidation(name) && CategoryValidation(category) && DesriptionValidation(description) && PriceValidation(price));
        }

        private static bool NameValidation(string name)
        {
            return true;
        }

        private static bool CategoryValidation(string category)
        {
            return true;
        }

        private static bool DesriptionValidation(string description)
        {
            return true;
        }

        private static bool PriceValidation(decimal price)
        {
            return true;
        }

    }
}