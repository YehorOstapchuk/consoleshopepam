﻿using System;
using System.Collections.Generic;
using System.Text;

namespace shopEPAM
{
    public class User
    {
        public static int LastNumber = -1;
        public int Id
        {
            get;
            private set;
        }
        public string FirstName
        {
            get;
            private set;
        }

        public string SecondName
        {
            get;
            private set;
        }

        public string NickName
        {
            get;
            private set;
        }

        public string Password
        {
            get;
            private set;
        }

        public string PhoneNumber
        {
            get;
            private set;
        }

        public UserType Status
        {
            get;
            private set;
        }

        private User (string firstName, string secondName, string nickname, string password, string phone, UserType status )
        {
            LastNumber++;
            Id = LastNumber;
            FirstName = firstName;
            SecondName = secondName;
            PhoneNumber = phone;
            Status = status;
            Password = password;
            NickName = nickname;
        }

        public static User Create(string firstName, string secondName, string nickname, string password, string phone = null, UserType status = UserType.Registered)
        {
            if (Validation(firstName, secondName, nickname, password, phone, status)) return new User(firstName, secondName, nickname, password, phone, status);
            else return null;
        }

        public bool ChangeFirstName(string firstName)
        {
            if (FirstNameValidation(firstName)) { FirstName = firstName; return true; }
            else return false;
        }

        public bool ChangeSecondName(string secondName)
        {
            if (SecondNameValidation(secondName)) { SecondName = secondName; return true; }
            else return false;
        }

        public bool ChangePhone(string phone)
        {
            if (PhoneValidation(phone)) { PhoneNumber = phone; return true; }
            else return false;
        }

        public bool ChangePassword(string password)
        {
            if (PasswordValidation(password)) { Password = password; return true; }
            else return false;
        }


        public bool ChangeNickName(string nick)
        {
            if (NickNameValidation(nick)) { NickName = nick; return true; }
            else return false;
        }

        public bool ChangeStatus(UserType status)
        {
            if (StatusValidation(status)) { Status = status; return true; }
            else return false;
        }

        private static bool Validation(string firstName, string secondName, string nick, string password, string phone, UserType status)
        {
            return (FirstNameValidation(firstName) && SecondNameValidation(secondName) && PhoneValidation(phone) && StatusValidation(status) && PasswordValidation(password) && NickNameValidation(nick));
        }

            private static bool FirstNameValidation(string firstName)
        {
            return true;
        }

        private static bool SecondNameValidation(string secondName)
        {
            return true;
        }

        private static bool PhoneValidation(string phone)
        {
            return true;
        }

        private static bool NickNameValidation(string phone)
        {
            return true;
        }

        private static bool StatusValidation(UserType status)
        {
            return true;
        }

        private static bool PasswordValidation(string password)
        {
            return true;
        }
    }
}
