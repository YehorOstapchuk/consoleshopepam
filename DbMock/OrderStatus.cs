﻿using System;
using System.Collections.Generic;
using System.Text;

namespace shopEPAM
{

    public enum OrderStatus
    {
        New,
        Sending,
        Sent,
        OnWay,
        DeliveredToPost,
        Denied,
        DeniedByAdmin,
        Completed
    }
}
