﻿using System;

namespace shopEPAM
{
    class Program
    {
        static void Main(string[] args)
        {
            Setup.SetData();
            Handler.MainHandle();
        }
    }
}
