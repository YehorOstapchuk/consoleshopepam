﻿using System;
using System.Collections.Generic;
using System.Text;

namespace shopEPAM
{
    public class GuestArgs : EventArgs
    {
        public string Find
        {
            get;
            set;
        }

        public string FirstName
        {
            get;
            set;
        }

        public string SecondName
        {
            get;
            set;
        }

        public string Password
        {
            get;
            set;
        }
        public string NickName
        {
            get;
            set;
        }
        public string PhoneNumber
        {
            get;
            set;
        }

        public RegisteredUserArgs CastToRegister ()
        {
            RegisteredUserArgs args = new RegisteredUserArgs();
            args.Find = this.Find;
            args.FirstName = this.FirstName;
            args.SecondName = this.SecondName;
            args.Password = this.Password;
            args.NickName = this.NickName;
            args.PhoneNumber = this.PhoneNumber;
            return args;
        }

        public AdminArgs CastToAdmin()
        {
            AdminArgs args = new AdminArgs();
            args.Find = this.Find;
            args.FirstName = this.FirstName;
            args.SecondName = this.SecondName;
            args.Password = this.Password;
            args.NickName = this.NickName;
            args.PhoneNumber = this.PhoneNumber;
            return args;
        }
    }
}
