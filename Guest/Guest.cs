﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace shopEPAM
{
     public static class Guest
    {

        public static void Menu()
        {
            Console.Write("Enter the number:\n" +
                "1 - Show All Goods\n" +
                "2 - Find Good With Name\n" +
                "3 - Register\n" +
                "4 - Log In \n");
            HandleMenu();
        }

        public static void HandleMenu()
        {
            try
            {
                int key;
                key = Int32.Parse(Console.ReadLine());
                if ((key < 1) || (key > 4)) { Console.WriteLine("Wrong Input"); return; }
                List<Action> action = new List<Action>();
                action.Add(First);
                action.Add(Second);
                action.Add(Third);
                action.Add(Fourth);
                action[key - 1].Invoke();
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }


        public static void First()
        {
            ShowGoods(new GuestArgs());
        }

        public static void Second()
        {
            GuestArgs args = Tools.GetFindData();
            FindGoodWithName(args);
        }

        public static void Third()
        {
            GuestArgs args = RegisterGetData();
            Register(args);
        }

        public static void Fourth()
        {
            GuestArgs args = LogInGetData();
            LogIn(args);
        }


        public static void ShowGoods(GuestArgs args)
        {
            Tools.ShowGoods();
        }

        public static void FindGoodWithName(GuestArgs args)
        {
            Tools.ShowFindGood(args);
        }

        public static void Register(GuestArgs args)
        {
            ShopDbMock.AddUser(args.FirstName, args.SecondName, args.NickName, args.Password, args.PhoneNumber, UserType.Registered);
        }

        public static void LogIn(GuestArgs args)
        {
            if (ShopDbMock.Users.Where(a => (a.NickName == args.NickName) && (a.Password == args.Password)).Count() < 1)
                throw new ArgumentException("Wrong Log In");
            else
            {
                Console.WriteLine("Authorized!");
                shopEPAM.Handler.Authorization(args);
            }
        }

        public static GuestArgs LogInGetData ()
        {
            GuestArgs args = new GuestArgs();
            Console.WriteLine("Enter your NickName:");
            args.NickName = Console.ReadLine();
            Console.WriteLine("Enter your Password:");
            args.Password = Console.ReadLine();
            return args;
        }


        public static GuestArgs RegisterGetData()
        {
            GuestArgs args = new GuestArgs();
            Console.WriteLine("Registration:");
            Console.WriteLine("Enter your First Name");
            args.FirstName = Console.ReadLine();
            Console.WriteLine("Enter your Second Name");
            args.SecondName = Console.ReadLine();
            Console.WriteLine("Enter your NickName:");
            args.NickName = Console.ReadLine();
            Console.WriteLine("Enter your Password:");
            args.Password = Console.ReadLine();
            Console.WriteLine("Do you want to enter your phone number? (Y/N)");
            string flag = Console.ReadLine();
            if (flag == "Y")
            {
                Console.WriteLine("Enter your phone:");
                args.PhoneNumber = Console.ReadLine();
            }
            return args;
        }

    }
}
