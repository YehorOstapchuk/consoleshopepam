﻿using System;
using System.Collections.Generic;
using System.Text;

namespace shopEPAM
{
    public class AdminArgs : RegisteredUserArgs
    {
        public User User
        {
            get;
            set;
        }

        public Order Order
        {
            get;
            set;
        }

        public Good Good
        {
            get;
            set;
        }

        public string GoodName
        {
            get;
            set;
        }

        public string GoodDescription
        {
            get;
            set;
        }

        public string GoodCategory
        {
            get;
            set;
        }

        public decimal GoodPrice
        {
            get;
            set;
        }

        public OrderStatus OrderStatus
        {
            get;
            set;
        }
    }
}
