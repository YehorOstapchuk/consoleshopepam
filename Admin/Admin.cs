﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace shopEPAM
{
    public class Admin
    {
        public static User user;
        public static void Menu()
        {
            Console.WriteLine("------------------------------");
            if (user == null) return;
            Console.WriteLine("Enter the number:\n" +
                "1 - Show All Goods\n" +
                "2 - Find Good With Name\n" +
                "3 - Create a new Order\n" +
                "4 - Check or deny your Order\n" +
                "5 - List of Users\n" +
                "6 - Change User Profile\n" +
                "7 - Make User Admin\n" +
                "8 - Add Good\n" +
                "9 - Change Good\n" +
                "10 - List of Orders\n" +
                "11 - Change Order Status\n" +
                "12 - Log Out\n");
            HandleMenu();
        }

        public static void HandleMenu()
        {
            try
            {
                int key;
                key = Int32.Parse(Console.ReadLine());
                if ((key < 1) || (key > 12)) { Console.WriteLine("Wrong Input"); return; }
                List<Action> action = new List<Action>();
                action.Add(First);
                action.Add(Second);
                action.Add(Third);
                action.Add(Fourth);
                action.Add(Fifth);
                action.Add(Sixth);
                action.Add(Seventh);
                action.Add(Eighth);
                action.Add(Nineth);
                action.Add(Tenth);
                action.Add(Eleventh);
                action.Add(Twelfth);
                action[key - 1].Invoke();
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public static void First()
        {
            ShowGoods(new AdminArgs());
        }

        public static void Second()
        {
                AdminArgs    args = Tools.GetFindData().CastToAdmin();
            FindGoodWithName(args);
        }

        public static void Third()
        {
            RegisteredUser.user = user;
            AdminArgs args = RegisteredUser.GetOrderData().CastToAdmin(); ;
            CreateOrder(args);
        }

        public static void Fourth()
        {
             RegisteredUser.user = user;
             AdminArgs args = RegisteredUser.GetUserOrdersData().CastToAdmin();
            ChangeOrderStatus(args);
        }

        public static void Fifth()
        {

            ShowUsers(new AdminArgs());
        }

        public static void Sixth()
        {
            AdminArgs args = new AdminArgs();
            args.User = GetUserById(GetUserData());
            if (args.User == null) { Console.WriteLine("Not Found"); return; }
            else ChangeProfile(args);
        }

        public static void Seventh()
        {
            AdminArgs args = new AdminArgs();
            args.User = GetUserById(GetUserData());
            if (args.User == null) { Console.WriteLine("Not Found"); return; }
            else MakeAdmin(args);
        }

        public static void Eighth()
        {
            AdminArgs args = GetGoodData();
            //Console.WriteLine("Name:", args.GoodName);
            AddGood(args);
        }

        public static void Nineth()
        {
            AdminArgs args = new AdminArgs();
             args.Good = Tools.FindByIdGood(GetGoodIdData());
            if (args.Good == null) { Console.WriteLine("Not Found"); return; }
            else ChangeGood(args);
        }

        public static void Tenth()
        {
            ShowOrders(new AdminArgs());
        }

        public static void Eleventh()
        {
            AdminArgs args = new AdminArgs();
             args.Order = Tools.FindByIdOrder(GetOrderIdData());
             if (args.Order == null) { Console.WriteLine("Not Found"); return; }
             else ChangeOrdersStatus(args);
        }

        public static void Twelfth()
        {
            LogOut(new AdminArgs());
        }

        public static void ChangeOrdersStatus(AdminArgs args)
        {
            Order temp = args.Order;
            args = Tools.GetDataToChangeOrderStatus();
            temp.ChangeStatus(args.OrderStatus);
        }
        public static void ShowOrders(AdminArgs args)
        {
            var orders = Tools.GetAllOrders();
            foreach(Order order in orders)
            {
                Tools.DisplayOrderForAdmin(order);
            }
        }

        public static void ChangeGood(AdminArgs args)
        {
            Good temp = args.Good;
            args = Tools.GetDataToChangeGood();
            Tools.ChangeGood(args, temp);
        }

        public static int GetOrderIdData()
        {
            Console.WriteLine("Enter the order id");
            int id = Int32.Parse(Console.ReadLine());
            return id;
        }

        public static int GetGoodIdData()
        {
            Console.WriteLine("Enter the good id");
            int id = Int32.Parse(Console.ReadLine());
            return id;
        }

        public static void AddGood (AdminArgs args)
        {
            ShopDbMock.AddGood(args.GoodName, args.GoodPrice, args.GoodCategory, args.GoodDescription);
        }

        public static AdminArgs GetGoodData()
        {
            AdminArgs args = new AdminArgs();
            Console.WriteLine("Enter the name of Good:");
            args.GoodName = Console.ReadLine();
            Console.WriteLine("Enter the good category:");
            args.GoodCategory = Console.ReadLine();
            Console.WriteLine("Enter the good description");
            args.GoodDescription = Console.ReadLine();
            Console.WriteLine("Enter the Good Price");
            args.GoodPrice = Decimal.Parse(Console.ReadLine());
            Console.WriteLine("Name:", args.GoodName);
            return args;
        }


        public static void LogOut(AdminArgs args)
        {
            user = null;
            shopEPAM.Handler.LogOut();
        }

        public static void MakeAdmin(AdminArgs args)
        {
            args.User.ChangeStatus(UserType.Administrator);
        }
        public static int GetUserData()
        {
            Console.WriteLine("Enter the user id");
            int id = Int32.Parse(Console.ReadLine());
            return id;
        }
        public static User GetUserById(int id)
        {
            return ShopDbMock.Users.Where(a => a.Id == id).FirstOrDefault();
        }

        public static void ChangeProfile(AdminArgs args)
        {
            User temp = args.User;
            args = Tools.GetDataToChangeProfile().CastToAdmin();
            Tools.ChangeUser(args, temp);
        }

        public static void ShowUsers(AdminArgs args)
        {
            foreach (User user in Tools.GetAllUsers())
            {
                Tools.DisplayUser(user);
            }
        }

        public static void ShowGoods(AdminArgs args)
        {
            RegisteredUser.ShowGoods(args);
        }

        public static void FindGoodWithName(AdminArgs args)
        {
            RegisteredUser.FindGoodWithName(args);
        }

        public static void CreateOrder(AdminArgs args)
        {
            RegisteredUser.CreateOrder(args);
        }

        public static void ChangeOrderStatus(AdminArgs args)
        {
            RegisteredUser.ChangeOrderStatus(args);
        }

    }
}
