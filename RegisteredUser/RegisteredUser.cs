﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace shopEPAM
{
    public static class RegisteredUser
    {
        public static User user;


        public static void Menu()
        {
            if (user == null) return;
            Console.WriteLine("------------------------------");
            Console.WriteLine("Enter the number:\n" +
                "1 - Show All Goods\n" +
                "2 - Find Good With Name\n" +
                "3 - Create a new Order\n" +
                "4 - Complete or deny Order\n" +
                "5 - List of Orders\n" +
                "6 - List of Completed orders\n" +
                "7 - Change your profile\n" +
                "8 - LogOut\n");
            HandleMenu();
        }

        public static void HandleMenu()
        {
            try
            {
                int key;
                key = Int32.Parse(Console.ReadLine());
                if ((key < 1) || (key > 8)) { Console.WriteLine("Wrong Input"); return; }
                List<Action> action = new List<Action>();
                action.Add(First);
                action.Add(Second);
                action.Add(Third);
                action.Add(Fourth);
                action.Add(Fifth);
                action.Add(Sixth);
                action.Add(Seventh);
                action.Add(Eighth);
                action[key - 1].Invoke();
            }
            catch (ArgumentException ex)
            {
                Console.WriteLine(ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public static void First()
        {
            ShowGoods(new RegisteredUserArgs());
        }

        public static void Second()
        {
            RegisteredUserArgs args = Tools.GetFindData().CastToRegister();
            FindGoodWithName(args);
        }

        public static void Third()
        {
            RegisteredUserArgs args = GetOrderData();
            CreateOrder(args);
        }

        public static void Fourth()
        {
            RegisteredUserArgs args = GetUserOrdersData();
            ChangeOrderStatus(args);
        }

        public static void Fifth ()
        {
            ShowOrders(new RegisteredUserArgs());
        }

        public static void Sixth()
        {
            ShowCompletedOrders(new RegisteredUserArgs());
        }

        public static void Seventh()
        {
           RegisteredUserArgs args = Tools.GetDataToChangeProfile().CastToRegister();
            ChangeProfile(args);
        }

        public static void Eighth()
        {
            LogOut(new RegisteredUserArgs());
        }

        public static void ChangeProfile (RegisteredUserArgs args)
        {
            Tools.ChangeUser(args, user);
        }

        public static void ChangeOrderStatus (RegisteredUserArgs args)
        {
            Console.WriteLine("OrderId", args.OrderId);
            Order order = Tools.FindByIdOrder(args.OrderId);
            if (args.Confirm) order.ChangeStatus(OrderStatus.Completed);
            else order.ChangeStatus(OrderStatus.Denied);
        }

        public static RegisteredUserArgs GetUserOrdersData ()
        {
            IEnumerable < Order > orders = Tools.GetAllUserOrdersWithStatus(user.Id, OrderStatus.New);
            foreach (Order order in orders)
            {
                Tools.DisplayOrderForUser(order);
            }
            IEnumerable<Order> orders2 = Tools.GetAllUserOrdersWithStatus(user.Id, OrderStatus.DeliveredToPost);
            foreach (Order order in orders2)
            {
                Tools.DisplayOrderForUser(order);
            }
            Console.WriteLine("Write the id of the order:");
            int id;
            id =Int32.Parse( Console.ReadLine());
            if ((orders.Where(a => a.Id == id).ToArray().Length != 1) && (orders2.Where(a => a.Id == id).ToArray().Length != 1))  throw new ArgumentException("Not Found");
            RegisteredUserArgs args = new RegisteredUserArgs();
            args.OrderId = id;
            int flag;
            Console.WriteLine("Enter 1 - to Complete, 2 - to Deny:");
            flag = Int32.Parse(Console.ReadLine());
            if (flag == 1) args.Confirm = true;
            else args.Confirm = false;
            return args;
        }

        public static void LogOut (RegisteredUserArgs args)
        {
            user = null;
            shopEPAM.Handler.LogOut();
        }

        public static void ShowCompletedOrders(RegisteredUserArgs args)
        {
            foreach (Order order in Tools.GetAllUserOrdersWithStatus(user.Id, OrderStatus.Completed))
            {
                Tools.DisplayOrderForUser(order);
            }
        }

        public static void ShowOrders(RegisteredUserArgs args)
        {
            if (Tools.GetAllUserOrders(user.Id).Count() == 0)
            {
                Console.WriteLine("No orders");
                return;
            }
            foreach(Order order in Tools.GetAllUserOrders(user.Id))
            {
                Tools.DisplayOrderForUser(order);
            }
        }

        public static void ShowGoods(RegisteredUserArgs args)
        {
            Guest.ShowGoods(args);
        }

        public static void FindGoodWithName(RegisteredUserArgs args)
        {
            Guest.FindGoodWithName(args);
        }

        public static void CreateOrder (RegisteredUserArgs args)
        {
            ShopDbMock.AddOrder(user, Tools.FindByIdGood(args.GoodId));
        }

        public static RegisteredUserArgs GetOrderData()
        {
            RegisteredUserArgs args = new RegisteredUserArgs();
            Console.WriteLine("Enter the Good's Id:");
            args.GoodId = Int32.Parse(Console.ReadLine());
            return args;
        }
    }
}
