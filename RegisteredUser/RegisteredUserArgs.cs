﻿using System;
using System.Collections.Generic;
using System.Text;

namespace shopEPAM
{
    public class RegisteredUserArgs : GuestArgs
    {
        public int GoodId
        {
            get;
            set;
        }

        public int OrderId
        {
            get;
            set;
        }

        public bool Confirm
        {
            get;
            set;
        }

        public new AdminArgs CastToAdmin()
        {
            AdminArgs args = new AdminArgs();
            args.Find = this.Find;
            args.FirstName = this.FirstName;
            args.SecondName = this.SecondName;
            args.Password = this.Password;
            args.NickName = this.NickName;
            args.PhoneNumber = this.PhoneNumber;
            args.GoodId = this.GoodId;
            args.OrderId = this.OrderId;
            args.Confirm = this.Confirm;
            return args;
        }
    }
}
