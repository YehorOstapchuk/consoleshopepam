﻿using System;
using System.Collections.Generic;
using System.Text;

namespace shopEPAM
{
    public static class Setup
    {
        public static void SetData()
        {
            User temp = User.Create("Yehor", "Ostapchuk", "EgorOst", "zx2000", null, UserType.Administrator);
            ShopDbMock.AddUser(temp);
            temp = User.Create("Ivan", "Ivanov", "SuperVania", "v2000", null, UserType.Registered);
            ShopDbMock.AddUser(temp);
            temp = User.Create("Petr", "Petrov", "SuperPeyia", "p2000", null, UserType.Registered);
            ShopDbMock.AddUser(temp);
            temp = User.Create("Admin", "Admin", "Admin", "123456", null, UserType.Administrator);
            ShopDbMock.AddUser(temp);
            Good good1 = Good.Create("Iphone 11", 1000, "Phones", "The newest Iphone");
            Good good2 = Good.Create("IphoneX", 700, "Phones", "Good Phone");
            Good good3 = Good.Create("Samsung S10", 800, "Phones", "The Best Android Phone");
            ShopDbMock.AddGood(good1);
            ShopDbMock.AddGood(good2);
            ShopDbMock.AddGood(good3);

        }

        public static void DbReset()
        {
            RegisteredUser.user = null;
            Admin.user = null;
            Order.LastNumber = -1;
            User.LastNumber = -1;
            Good.LastNumber = -1;

            ShopDbMock.Users = new List<User>();
            ShopDbMock.Goods = new List<Good>();
            ShopDbMock.Orders = new List<Order>();
            ShopDbMock.UserAmount = 0;
            ShopDbMock.GoodsAmount = 0;
            ShopDbMock.OrdersAmount = 0;
        }
    }
}
