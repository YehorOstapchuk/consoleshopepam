﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace shopEPAM
{
    public static class Handler
    {
        public delegate void Menu();

        public static event Menu menu;

        static Handler()
        {
            menu = Guest.Menu;
        }
        public static void MainHandle()
        {
            while (true) menu.Invoke();
        }

        public static void Authorization (GuestArgs args)
        {
            User temp = ShopDbMock.Users.Where(a => (a.NickName == args.NickName) && (a.Password == args.Password)).FirstOrDefault();
            if (temp.Status == UserType.Registered)
            {
                RegisteredUser.user = temp;
                menu = RegisteredUser.Menu;
            }
            else
            {
                Admin.user = temp;
                menu = Admin.Menu;
            }
        }
        
        public static void LogOut ()
        {
            menu = Guest.Menu;
        }
    }
}
